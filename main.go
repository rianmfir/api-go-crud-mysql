package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func connect() *sql.DB {
	user := "root"
	password := "password"
	host := "localhost"
	port := "3306"
	database := "db_bank"

	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, password, host, port, database)

	db, err := sql.Open("mysql", connection)

	if err != nil {
		log.Fatal(err)
	}

	return db
}

type Customer struct {
	Id      string `json:"id"`
	Nama    string `json:"nama"`
	Alamat  string `json:"alamat"`
	Telepon string `json:"telepon"`
}

var Customers []Customer

func returnAllCustomers(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllCustomers")

	var customer Customer
	var arr_customer []Customer
	// var response Response

	db := connect()
	defer db.Close()

	rows, err := db.Query("Select * from tb_customer")
	if err != nil {
		log.Print(err)
	}

	for rows.Next() {
		if err := rows.Scan(&customer.Id, &customer.Nama, &customer.Alamat, &customer.Telepon); err != nil {
			log.Fatal(err.Error())

		} else {
			arr_customer = append(arr_customer, customer)
		}
	}

	log.Print("Show Customers")

	Customers = arr_customer

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(Customers)

}

func returnSingleCustomers(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnSingleCustomers")

	var customer Customer
	var arr_customer []Customer
	// var response Response

	db := connect()
	defer db.Close()

	id := r.FormValue("id")
	rows, err := db.Query("Select * from tb_customer where cust_id = ?",
		id,
	)

	if err != nil {
		log.Print(err)
	}

	for rows.Next() {
		if err := rows.Scan(&customer.Id, &customer.Nama, &customer.Alamat, &customer.Telepon); err != nil {
			log.Fatal(err.Error())

		} else {
			arr_customer = append(arr_customer, customer)
		}
	}

	log.Print("Show Customer")

	Customers = arr_customer

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(Customers)
}

func addCustomers(w http.ResponseWriter, r *http.Request) {
	db := connect()
	defer db.Close()

	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err)
	}

	nama := r.FormValue("nama")
	alamat := r.FormValue("alamat")
	telepon := r.FormValue("telepon")

	_, err = db.Exec("INSERT INTO tb_customer (nama, alamat, telepon) values (?,?,?)",
		nama,
		alamat,
		telepon,
	)

	if err != nil {
		log.Print(err)
	}

	arr := []Customer{
		{
			Nama:    nama,
			Alamat:  alamat,
			Telepon: telepon,
		},
	}

	log.Print("Insert data to database")

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(arr)

}

func updateCustomers(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err)
	}

	id := r.FormValue("id")
	nama := r.FormValue("nama")
	alamat := r.FormValue("alamat")
	telepon := r.FormValue("telepon")

	_, err = db.Exec("UPDATE tb_customer set nama = ?, alamat = ?, telepon = ? where cust_id = ?",
		nama,
		alamat,
		telepon,
		id,
	)

	if err != nil {
		log.Print(err)
	}

	arr := []Customer{
		{
			Id:      id,
			Nama:    nama,
			Alamat:  alamat,
			Telepon: telepon,
		},
	}

	log.Print("Update data to database")

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(arr)

}

func deleteCustomers(w http.ResponseWriter, r *http.Request) {

	db := connect()
	defer db.Close()

	err := r.ParseMultipartForm(4096)
	if err != nil {
		panic(err)
	}

	id := r.FormValue("id")

	_, err = db.Exec("DELETE from tb_customer where cust_id = ?",
		id,
	)

	if err != nil {
		log.Print(err)
	}

	log.Print("Delete data to database")

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode("Success")

}

func handleRequests() {
	router := mux.NewRouter()
	router.HandleFunc("/api/", returnAllCustomers).Methods("GET")
	router.HandleFunc("/api/id/", returnSingleCustomers).Methods("GET")
	router.HandleFunc("/api/add", addCustomers).Methods("POST")
	router.HandleFunc("/api/update", updateCustomers).Methods("PUT")
	router.HandleFunc("/api/delete", deleteCustomers).Methods("DELETE")

	fmt.Println("Connected on server http://localhost/7000")
	log.Fatal(http.ListenAndServe(":7000", router))
}

func main() {
	handleRequests()
}
